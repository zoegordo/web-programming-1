My website consists of four pages:
general.html: This page is the home page of my website. It contains links to all
other pages and general info about the abominable snowman. I have a photo
of the abominable snowman in there for reference.

sightings.html: This page is for reporting sightings of the abominable snowman.
I used a form to record user's responses and get any necessary info.
It also contains links to all other pages.

stats.html: This page contains "statistics" for the most relevant and credible
reported sightings of the abominable snowman. I did not include a row span
or col span element because I do not believe they make sense for the particular
table layout. This table would obviously look much neater if I used CSS. I also
included a map of the Himalayan mountain range in which all of these
sightings took place.

proof.html: This page simply contains a video to serve as "proof" that
the abominable snowman exists, as well as a warning.
