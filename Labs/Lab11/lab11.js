export class Lab11 {
  testDefaultParameters(first,second = 100) {
    console.log(arguments);
    return {
      first: first,
      second: second
    };
  }
  testTemplateLiterals(firstName, middleName, lastName) {
    return `${firstName},${middleName},${lastName}`;
  }
  testMultiLineStrings() {
    return `Hello

      this
      has

      lines

`;
  }
  testSortWithArrowFunction(array) {
    return array.sort((a,b) => {
      if (a<b) {
        return -1;
      } else if (a>b) {
        return 1;
      } else {
        return 0;
      }
    });
  }

}
